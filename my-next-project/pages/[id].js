import Link from 'next/link'
import Head from 'next/head'
import Container from '../components/container'

import { getAllPostIds, getPostData } from './posts'

export default function Post({ postData }) {
   return (<>
      <Container>
              <Head>
         <title>Posts</title>
      </Head>
         {postData.id}
         <br />
         {postData.title}
         <br />
         {postData.date}
      </Container>
      <h1><Link href="/one">
               <a>one</a>
            </Link></h1><br/>
           
           <h1> <Link href="/two"><a>two</a></Link></h1>
           <h1> <Link href="/">
               <a>HOME</a>
            </Link></h1><br/>
      </> 
   )
}
export async function getStaticPaths() {
   const paths = getAllPostIds()
   return {
      paths,
      fallback: false
   }
}

export async function getStaticProps({ params }) {
   const postData = getPostData(params.id)
      return {
      props: {
         postData
      }
   }
}