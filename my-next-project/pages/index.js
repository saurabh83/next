import Link from 'next/link'
import Head from 'next/head'
import Router from 'next/router'
import colors from '../data/colors.json'

function HomePage(props) {
   return (
      <>
      <Head>
            <title>Welcome to Next.js!</title>
         </Head>
         <div>Welcome to Next.js!</div>
         <h1><Link href="first"><a>First Post</a></Link></h1>
         <h1><Link href="about"><a>about Post</a></Link></h1>
         <h1><Link href="zero"><a>Posts</a></Link></h1>
         <h1><Link href="env"><a>env</a></Link></h1>
         <br/>
         <span onClick={() => Router.push('/zero')}>Click me...</span>
         <br/>
         <span onClick={() => Router.push('/?counter=1', undefined, { shallow: true })}>Reload</span>
          <br/>
         <div>Next stars: {props.stars}</div>
         <img src="/logo.png" alt="random Logo" />
         {colors.map(color => (
        <Link href={`/${color.name}`}>
        <h2>{color.name}</h2>
        </Link>
        
      ))}
      
      </>	    
   )
}
export async function getServerSideProps(context) {
  const res = await fetch('https://api.github.com/repos/vercel/next.js')
  const json = await res.json()
  return {
     props: { stars: json.stargazers_count }
  }
}
// export async function getServerSideProps({ params }) {
//   // Make a request to get data about the color via our API
//   const res = await fetch(`http://www.color-api.com/${params.color}`)
//   const color = await fetch.json()
//   // return the data as props that will be passed to the Color component
//   return { props: { color } }
// }
export default HomePage